<?php
 session_start();
 require("adatok.php");
 require("databaseClass.php");
 $db = new db($dbhost, $dbname, $dbuser, $dbpass);

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/jquery_ui.css">
  <link rel="stylesheet" href="css/jquery.bootstrap-touchspin.css">
    
  <?php
    if(isset($_SESSION['uID']))
    {
      echo'<link rel="stylesheet" href="css/style.css">';
    }
    else
    {
      echo'<link rel="stylesheet" href="css/log_style.css">';
    }
  ?>

</head>
<body>
  <header>

  </header>
  <main>
    <?php 
      include('modal.php');
    ?>
  </main>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery_ui.js"></script>
    <script src="js/jquery.bootstrap-touchspin.js"></script>
    <script src="js/app.js"></script>
</body>
</html>