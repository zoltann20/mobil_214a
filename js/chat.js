$(document).ready(function(){
    $('.alert-info').delay(3000).fadeOut(1500);
    $('.alert-warning').delay(3000).fadeOut(1500);
    $('.alert-danger').delay(3000).fadeOut(1500);
    $('.alert-success').delay(3000).fadeOut(1500);
 


    $('#myTable').DataTable({
        "pageLength": 25,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false
        } ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Hungarian.json"
        }        
    } );
  
    tinymce.init({
        selector:'.editor',
        plugins: 'table',
        toolbar: 'undo redo | formatselect | bold italic | ' +
                 'alignleft aligncenter alignright alignjustify | indent outdent | ' +
                 'table tableinsertdialog tablecellprops tableprops ',
        language: 'hu_HU'
    });

    

    function loadusers()
    {
        $('#userlist').load('users/users_show.php');
    }

    function loadmessages()
    {
        $('#messagelist').load('message_list.php');
    }


    setInterval(function(){loadusers()}, 1000);
    setInterval(function(){loadmessages()}, 1000);
});

function change_state()
{
    var state = $('select[name="states"]').val();
    $.post("changestate.php", { param: state });
}

function message_send()
{
    var message = tinymce.get("editor").getContent();
    if($message != '')
    {
        $.post("sendmessage.php", {param : message}, function(result){
            $("#info").fadeIn(1500);
            $("#info").html(result);
            $("#info").delay(3000).fadeOut(1500);
        });
        tinymce.get("editor").setContent('');
    }
}

function message_delete(ID)
{
    $.post("message_delete.php", {param : ID}, function(result){
        $("#info").fadeIn(1500);
        $("#info").html(result);
        $("#info").delay(3000).fadeOut(1500);
    });
}

function like(ID)
{
    $.post("reactions.php", { message: ID, type: 1 });
}

function dislike(ID)
{
    $.post("reactions.php", { message: ID, type: 2 });
}