var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}




$(document).ready(function(){
    $('.alert-info').delay(3000).fadeOut(1500);
    $('.alert-warning').delay(3000).fadeOut(1500);
    $('.alert-danger').delay(3000).fadeOut(1500);
    $('.alert-success').delay(3000).fadeOut(1500);
 

    

    setTimeout(function(){
        $( "#accordion" ).accordion();
        $("input[name='counterTouchspin']").TouchSpin();
    }, 250);

    
    
});

function loader(url)
{
    dir = url.substring(0, url.indexOf('_'));
    if(dir)
    {
        file = dir + '/' +url + '.php';
    }
    else
    {
        file = url + '.php';
    }
    $('.content').load(file);
}




function loadList(){
    $('#back').hide();
    $('#summBtn').show();

    setTimeout(function(){
        console.log('LOADING');
        $('#shoppinglist').load('products_list.php');
    });

    
}

function change_state()
{
    var state = $('select[name="states"]').val();
    $.post("changestate.php", { param: state });
}


function showErr(){
    $("#showError").show();
    
}

function showTouchSpin(ID){
    console.log('HELLO');
    $(".counter-"+ID).toggle();
   
}

function addItem(ID){
    var checkBox = '#checkbox-'+ID;

    if ($(checkBox).is(":checked"))
    {
        $.post("product_insert.php", { productID: ID});
        console.log(checkBox, 'insert');
    }
    else
    {
        $.post("product_delete.php", { productID: ID});
        console.log(checkBox, 'delete');
    }
    var count = $("[type='checkbox']:checked").length;
    $('#summary').text(+ count +' tétel');
    console.log(count);

}

function updateCart(ID){
    var counterValue = '#value-'+ID;
    var defaulValue = 1;
    var amount = $(counterValue).val();

    $.post("product_update.php", {productID : ID, summary : amount})
    console.log(ID, 'update' + amount);
}


function summary()
{
    $('#shoppinglist').load('summary.php');
    $('#back').show();
    $('#summBtn').hide();
    console.log("summary");
}


function plus(ID){
    var i = 0;
    $('#plusAmount-'+ID).click(function(){
        i++;
    })
    console.log(i);
}

function changeAmountPlus(ID){
   
    var box = $('#amountValue-'+ID);
    var am = parseInt(box.text());
    am++;
    $.post("product_update.php", {productID : ID, summary : am})
    box.html(am++);

    getSumAmount(ID);

}

function changeAmountMinus(ID){
   
    var box = $('#amountValue-'+ID);
    var am = parseInt(box.text());
    am--;
    $.post("product_update.php", {productID : ID, summary : am})
    box.html(am--);
    
    if(am == -1)
    {
        $.post("product_delete.php",{productID: ID});
        $('#shoppinglist').load('summary.php');
    }
    getSumAmount(ID);
  
    

}

function getSumAmount(ID){
    $.post("cart_sum.php", {productID : ID})
    .done((data)=>{
        $("#sum-"+ID).html(data);
    })
    getAllSumm();
}


function getAllSumm()
{
    $.post("count_summ.php").done((data)=>{
        data = new Intl.NumberFormat('hu-HU', { style: 'currency', currency: 'HUF' }).format(data)
        $("#superSumm >span").html(data);
    })
}




