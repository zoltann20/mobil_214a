<div id="modals">
<div id="loginBox">
  <h2>Mobile Login Form</h2>
  <div id="buttons">
    <button id="btn1" onclick="document.getElementById('id01').style.display='block'" class="buttonstyle">Bejelentkezés</button>
    <button id="btn2" onclick="document.getElementById('id02').style.display='block'" class="buttonstyle">Regisztráció</button>
  </div>
</div>  
  
    <div id="id01" class="modal">
      
      <form class="modal-content animate" action="users_login.php" name ="loginForm" method="post">
      <div class="imgcontainer">
          <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>

        <div class="container">
          <label for="uname"><b>Felhasználónév</b></label>
          <input type="text" placeholder="Felhasználónév" name="uname" required>

          <label for="psw"><b>Password</b></label>
          <input type="password" placeholder="Jelszó" name="psw" required>
            
          <button type="submit" class="buttonstyle">Bejelentkezés</button>
          <div id="showError" style="display:none;"><p>Hiba a bejelentkezés közben!</p></div>
          <label>
            <input type="checkbox"  name="remember"> Emlékezzen rám
          </label>
        </div>

        <div class="container" style="background-color:#f1f1f1">
          <button type="button" onclick="document.getElementById('id01').style.display='none'" class="buttonstyle">Vissza</button>
          <span class="psw"><a href="#" style="text-decoration: none; ">Elfelejtett jelszó</a></span>
        </div>
      </form>
    </div>

    <!-- REGISZTRÁCIÓ -->
    <div id="id02" class="modal">
      
      <form class="modal-content animate" action="users_new.php" method="post">
        <div class="imgcontainer">
          <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>

        <div class="container">
          <label for="uname" ><b>Felhasználónév</b></label>
          <input type="text" placeholder="Felhasználónév" name="uname" required>

          
         <label for="email"><b>Email cím </b></label>
         <input type="email" placeholder="Email cím" name="email" required>
           
          <label for="psw"><b>Jelszó</b></label>
          <input type="password" placeholder="Jelszó" name="psw" required>
          
          <label for="psw"><b>Jelszó megerősítés</b></label>
          <input type="password" placeholder="Jelszó megherősítése" name="psw2" required>

          <input type="submit" class="buttonstyle" name="reg" value="Regisztráció">
        </div>
      </form>
    </div>
  </div>