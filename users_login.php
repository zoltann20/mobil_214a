<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery_ui.css">
<link rel="stylesheet" href="css/jquery.bootstrap-touchspin.css">
<link rel="stylesheet" href="css/style.css">

<script src="js/jquery.min.js"></script>
<script src="js/jquery_ui.js"></script>
<script src="js/jquery.bootstrap-touchspin.js"></script>
<script src="js/app.js"></script>

<?php 
    session_start();
    require("adatok.php");
    require("databaseClass.php");
    $db = new db($dbhost, $dbname, $dbuser, $dbpass);
   
    // ha rákattintottunk a belépés gombra
    $pass = escapeshellcmd($_POST['psw']);
    $name = escapeshellcmd($_POST['uname']);
    if (empty($name) || empty($pass))
    {
        $db->showMessage('Nem adtál meg minden adatot!','danger');
    }
    else
    {
        $db->DBquery("SELECT * FROM users WHERE name='$name'");
        if ($db->numRows() == 0)
        {
           
           header("Location:showMessage.php");
        }
        else
        {
            $res = $db->fetchOne();
            if ($res['password'] != SHA1($pass))
            {
               
               header("Location:showMessage.php");
            }
            else
            {
                $db->DBquery("UPDATE users SET last = CURRENT_TIMESTAMP WHERE ID=".$res['ID']);

                $db->DBquery("SELECT * FROM users WHERE name='$name'");
                $res = $db->fetchOne();

                $_SESSION['uID'] = $res['ID'];
                $_SESSION['uname'] = $res['name'];
                $_SESSION['umail'] = $res['email'];
                $_SESSION['ureg'] = $res['reg'];
                $_SESSION['ulast'] = $res['last'];

                // létrehozzuk a sütiket a munkamenet változók alapján (30 napra)
                // setcookie('uid', $_SESSION['uID'], time() + (86400 * 30), '/');
                // setcookie('uname', $_SESSION['uname'], time() + (86400 * 30), '/');
                // setcookie('umail', $_SESSION['umail'], time() + (86400 * 30), '/');
                // setcookie('ureg', $_SESSION['ureg'], time() + (86400 * 30), '/');
                // setcookie('ulast', $_SESSION['ulast'], time() + (86400 * 30), '/');
                echo '<div id="shoppinglist"></div>';
                echo '<a  href="#" style="text-decoration: none" onclick="summary();" class="buttonstyle" id="summBtn">Összesítés</a>';
                echo '<a  href="#"  style="text-decoration: none" style="display:none;" id="back" onclick="loadList();" class="buttonstyle">Vissza</a>';
                echo '<a class="buttonstyle" style="text-decoration: none" href="index.php?pg="'.base64_encode('users_logout').'">Kijelentkezés</a>';
       
            }
        }
    }
?>

<script>loadList();</script>