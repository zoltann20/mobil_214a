<?php
    session_start();
    require 'adatok.php';
    require 'databaseClass.php';
    $db = new db($dbhost, $dbname, $dbuser, $dbpass);
    
    $db->DBquery("SELECT 
    products.ID AS 'ID',
    products.name AS 'name',
    carts.amount AS 'amount',
    products.price AS 'price'
    FROM carts
    INNER JOIN products ON products.ID = carts.productID
    WHERE userID=".$_SESSION['uID']);
    $result = $db->fetchAll();
   
    $db->DBquery("DELETE FROM carts WHERE amount=0");
    
    $sum = 0;
    $db->DBquery("SELECT email FROM users WHERE ID=".$_SESSION['uID']);
    $r = $db->fetchOne();

    echo '<h3>Kosár</h3>';
    echo '<span>'.$r['email'].'</span>';
    

    
    echo '<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Termék neve</th>
            <th scope="col" class="text-right">Ár</th>
            <th scope="col" class="text-right">Összesen</th>
            <th scope="col" class="text-right">Darabszám</th>
        </tr>
    </thead>';
    $i = 1;
    $summary = 0;
    foreach($result as $r)
    {
        $sum = $r['price']* $r['amount'];
        $summary +=$r['price']* $r['amount'];
        echo '<tbody class="summTable">
        <tr>
            <th scope="row">'.$i.'</th>
            <td>'.$r['name'].'</td>
            <td class="text-right">'.$r['price'].'</td>
            <td id="sum-'.$r['ID'].'" class="text-right">'.$sum.'</td>
            <td class="text-right">
            <a href="#" id="plusAmount-'.$r['ID'].'"  onclick="changeAmountPlus('.$r['ID'].');">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"  class="bi bi-plus" viewBox="0 0 16 16">
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                </svg>
            </a>
                <span id="amountValue-'.$r['ID'].'">'.$r['amount'].'</span> 
            <a href="#" id="minusAmount-'.$r['ID'].'"  onclick="changeAmountMinus('.$r['ID'].');">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash" viewBox="0 0 16 16">
                <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
            </svg>
            </a></td>
        </tr>';
        $i++;
    }
    echo '  
    </tbody>
    </table>';
    echo '<div class="container-fluid" id="superSumm">Összesen: <span >'.$db->numberFormat($summary).''.$GLOBALS['penznem'].'</span></div>';
        
?>