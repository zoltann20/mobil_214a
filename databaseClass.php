<?php
    // Bajai SZC Türr István Technikum
    // 2/14.A. Szoftverfejlesző
    // 2020/21.
    // Objektum orientált MySQL Adatbázis osztály 

    class db 
    {
        // attributes
        public $version; // az osztály verziószáma
        private $dbhost; // az adatbázis kiszolgáló címe
        private $dbname; // az adatbázis neve
        private $dbuser; // az adatbázis felhasználó neve
        private $dbpass; // az adatbázis felhasználó jelszava
        private $connection; // az adatbázis kapcsolat
        public $queryresult; // az SQL lekérdezések eredménye
        public $querycount = 0; // az elvégzett lekérdezések száma
        private $debug = false; // hibakeresési mód
        // methods
        
        public function __construct($host, $name, $user, $pass) // az osztály konstruktora
        {
            // az átvett paramétereket átadjuk az objektum megfelelő attributumainak
            $this->dbhost = $host; 
            $this->dbname = $name;
            $this->dbuser = $user;
            $this->dbpass = base64_decode($pass);

            // az objektumunk connection attributuma egy új mysqli objektum lesz
            $this->connection = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);

            // ha nem jön létre a mysqli objektum (nem épül fel az adatbázis kapcsolat), akkor kiírjuk a hibaüzenetet
            if ($this->connection->connect_error)
            {
                die("Hiba történt az adatbázis kapcsolat felépítése közben! (" .$this->connection->connect_error.")");
            }

            // beállítjuk a karakterkódolást unicode-ra
            $this->connection->set_charset("utf8");
            // beállítjuk a verziószámot
            $this->version = "v1.210208";
        }

        public function __destruct() // az osztály destruktora
        {
            // egyenlőre nincs funkciója
        }

        // az SQL lekérdezésekhez szükséges metódus
        public function DBquery($sql)
        {
            // lefuttatjuk a mysql osztály query metódusát és a kapott eredményeket a queryresult attributumban tároljuk
            if ($this->queryresult = $this->connection->query($sql))
            {
                // ha sikeres a lekérdezés, akkor a querycount változót növeljük
                // echo 'Sikeres lekérdezés!';
                $this->querycount++;
                if ($this->debug)
                {
                    $this->showMessage($sql,'primary');
                }
            }
            else
            {
                // ha nem sikerül lefuttatni a lekérdezést, akkor kiíratjuk a hibaüzenetet
                die('Hiba az adatbázis lekérdezés futtatása során! ('.$this->connection->error.')
                <div class="alert alert-warning">'.$sql.'</div>');
            }
            // visszaadjuk az eredményeket
            return $this->queryresult;
        }

        // lekérdezésben szereplő rekodrok kigyűjtése 
        public function fetchAll()
        {
            $result = array();
            foreach($this->queryresult as $rekord)
            {
                $result[] = $rekord;
            }
            return $result;
        }

        // egyetlen rekord kigyűjtése
        public function fetchOne()
        {
            foreach($this->queryresult as $rekord)
            {
                $result = $rekord;
            }
            return $result;            
        }

        // a lekérdezésben szereplő rekordok száma
        public function numRows()
        {
            return $this->queryresult->num_rows;
        }

        // a lekérdezésben szereplő mezők száma
        public function numFields()
        {
            return $this->queryresult->field_count;
        }

        // a lekérdezésben szereplő utolsó ID (auto increment) mező értéke
        public function lastID()
        {
            return $this->connection->insert_id;
        }

        // a lekérdezésben szereplő következő ID (auto increment) mező értéke
        public function nextID()
        {
            return $this->connection->insert_id+1;
        }

        // dinamikus űrlap
        public function toForm($param)
        {
          
            $input_elements = array();
            $method = 'POST';
            $enctype = '';

            // kiszedjük a sortöréseket
            $param = trim(preg_replace('/\s\s+/', '', $param));

            $params = explode('¤', $param);
            foreach($params as $value)
            {
                $tags = explode('|', $value);

                if (!isset($tags[2])){ $tags[2] = '';}
                if (!isset($tags[3])){ $tags[3] = '';}
                if (!isset($tags[4])){ $tags[4] = '';}
                if (!isset($tags[5])){ $tags[5] = '';}
                if (!isset($tags[6])){ $tags[6] = '';}

                $o = '<div class="form-group">';
                $c = '</div>';

                if(!empty($tags[1]))
                {
                    //ha .-al kezdődik a neve akkor nincs open tag
                    if($tags[1][0] == '_')
                    {
                        $o = '';
                    }
                    //ha .-ra végződik a neve akkor nincs close tag
                    if($tags[1][strlen($tags[1])-1] == '_')
                    {
                        $c = '';
                    }
                }

                switch($tags[0])
                {
                    case 'name': {
                        if(!empty($tags[1]))
                        {
                            $title = '<h4 class="animated slideInDown faster">'.$tags[1].'</h4><hr class="animated zoomIn">';
                        }
                        else
                        {
                            $title = '';
                        }
                        break;
                    }
                    case 'action': {
                        if ($tags[1] == 'index')
                        {
                            $action = 'index.php';
                        }
                        else
                        {
                            $action = 'index.php?pg='.base64_encode($tags[1]);
                        }
                        break;
                    }
                    case 'method':{
                        $method = $tags[1];
                        break;
                    }
                    case 'a' : {
                        $input_elements[] = $o.'<a href="index.php?pg='.base64_encode($tags[1]).'">'.$tags[2].'</a>'.$c;
                        break;
                    }
                    case 'label' : {
                        $input_elements[] = $o.'<label for="'.$tags[1].'">'.$tags[2].'</label>'.$c;
                        break;
                    }
                    case 'text' : {
                        $input_elements[] = $o.'<input type="text" name="'.$tags[1].'" placeholder="'.$tags[2].'" class="form-control" '.$tags[3].'>'.$c;
                        break;
                    }
                    case 'hidden' : {
                        $input_elements[] = $o.'<input type="hidden" name="'.$tags[1].'" value="'.$tags[2].'">'.$c;
                        break;
                    }
                    case 'textarea' : {
                        $input_elements[] = $o.'<textarea name="'.$tags[1].'" class="form-control '.$tags[3].'" id="'.$tags[3].'">'.$tags[2].'</textarea>'.$c;
                        break;
                    }
                    case 'email' : {
                        $input_elements[] = $o.'<input type="email" name="'.$tags[1].'" placeholder="'.$tags[2].'" class="form-control" '.$tags[3].'>'.$c;
                        break;
                    }
                    case 'password' : {
                        $input_elements[] = $o.'<input type="password" name="'.$tags[1].'" placeholder="'.$tags[2].'" class="form-control">'.$c;
                        break;
                    }
                    case 'button' : {
                        if(empty($tags[3])) $tags[3] = 'primary';
                        if(!empty($tags[4])) $tags[4] = 'onclick="'.$tags[4].'"';
                        $input_elements[] = $o.'<input type="button" name="'.$tags[1].'" value="'.$tags[2].'" class="btn btn-'.$tags[3].'" '.$tags[4].'>'.$c;
                        break;
                    }
                    case 'iconbutton' : {
                        $icon = $tags[1];
                        if($tags[1][0] == '_')
                        {
                            $icon = substr($icon, 1);
                        }
                        //ha .-ra végződik a neve akkor nincs close tag
                        if($tags[1][strlen($icon)-1] == '_')
                        {
                            $icon = substr($icon, 0, strlen($icon)-1);
                        }

                        $input_elements[] = $o.'<button name="'.$tags[1].'" class="btn btn-outline-primary">
                        <svg class="bi" width="20" height="20" fill="currentColor">
                        <use xlink:href="icons/bootstrap-icons.svg#'.$icon.'"></use>
                        </svg>
                        &nbsp;&nbsp;&nbsp;'.$tags[2].'</button>'.$c;
                        break;
                    }
                    case 'submit' : {
                        if(empty($tags[3]))
                        {
                            $tags[3] = 'primary';
                        }
                        $input_elements[] = $o.'<input type="submit" name="'.$tags[1].'" value="'.$tags[2].'" class="btn btn-'.$tags[3].'">'.$c;
                        break;
                    }
                    case 'checkbox' : {
                        $input_elements[] = $o.'<input type="checkbox" name="'.$tags[1].'" '.$tags[3].'> '.$tags[2].$c;
                        break;
                    }
                    case 'radio' : {
                        $input_elements[] = $o.'<input type="radio" name="'.$tags[1].'" '.$tags[3].'> '.$tags[2].$c;
                        break;
                    }
                    case 'number' : {
                        $input_elements[] = $o.'<input type="number" name="'.$tags[1].'" '.$tags[3].'">'.$c;
                        break;
                    }
                    case 'date' : {
                        $input_elements[] = $o.'<input type="date" name="'.$tags[1].'" class="form-control" '.$tags[3].'>'.$c;
                        break;
                    }
                    case 'select' : {
                        $input_elements[] =  $o.$this->toSelect($tags[1], $tags[2], $tags[3], $tags[4], $tags[5], $tags[6]).$c;
                        break;
                    }
                    case 'file' : {
                        $input_elements[] = $o.'<input type="file" name="'.$tags[1].'" id="file" accept="'.$tags[2].'" '.$tags[3].' class="form-control" onchange="uploadFile()">'.$c;
                        $enctype = 'enctype="multipart/form-data"';
                        break;
                    }
                }
            }
            echo $title.'<form method="'.$method.'" action="'.$action.'" '.$enctype.' class="animated fadeIn">';
            foreach($input_elements as $element)
            {
                echo $element;
            }
            echo '</form>';
        }

        // a lekérdezés adatainak megjelenítése egy HTML táblázatban
        public function toTable($param)
        {
            $fields = array();
            $fields = $this->queryresult->fetch_fields();
            $fieldcount = $this->connection->field_count;
            $rekordcount = $this->numRows();
            $tablename = $fields[0]->table;
            $originaltablename = $fields[0]->orgtable;
            $primary_key = $this->getPrimaryKey($fields);
            $SetFlagFieldName = $this->getSetFlag($fields);
            
            $actions = array();
            if (!empty($param))
            {
                $actions = explode("|", $param);
            }
            
            echo '
            <h4>'.ucfirst($tablename).' listája</h4>
            <hr>';
            if (!empty($actions))
            {                
                // ha a paraméterben van 'c',akkor megjelenítjük az új rekord felvétel gombot
                if (in_array('c', $actions))
                {
                    echo '<a href="index.php?pg='.base64_encode($originaltablename.'_new').'" class="btn btn-primary">Új rekord felvétele</a>';
                }
            }
            echo '<table id="myTable" class="table table-striped table-hover">
                <thead class="thead-dark">
                    <tr>';
                    foreach($fields as $field)
                    {
                        if ($field->name[0] == '.')
                        {
                            $cl = 'collapse';
                        }
                        else
                        {
                            $cl = '';
                        }
                        if (($field->type == 3) || ($field->type == 5))
                        {
                            echo '<th class="text-right '.$cl.'">'.$field->name.'</th>';
                        }
                        else
                        {
                            echo '<th class="'.$cl.'">'.$field->name.'</th>';
                        }
                    }   
                    if (!empty($actions))
                    {
                        $fieldcount++;
                        echo '<th class="text-right no-sort">Műveletek</th>';
                    }
                    echo '</tr>
                </thead>
                <tbody>';
                foreach($this->queryresult as $rekord)
                {
                    if (isset($rekord[$SetFlagFieldName]))
                    {
                        if (($rekord[$SetFlagFieldName] == 1))
                        {
                            $inaktive = '';
                            $icon = 'on';
                        }
                        else
                        {
                            $inaktive = 'class="text-muted"';   
                            $icon = 'off';
                        }
                    }
                    else
                    {
                        $inaktive = '';
                    }

                    if(isset($rekord['.def']))
                    {
                        if($rekord['.def'] == 1)
                        {
                            $deficon = 'check-circle-fill';
                        }
                        else
                        {
                            $deficon = 'circle';
                        }
                    }
                    else
                    {
                        $deficon = '';
                    }
                    
                    echo '<tr '.$inaktive.'>';
                    foreach($fields as $field)
                    {
                        if ($field->name[0] == '.')
                        {
                            $cl = 'collapse';
                        }
                        else
                        {
                            $cl = '';
                        }

                        if (is_numeric($rekord[$field->name]))
                        {
                            echo '<td class="text-right '.$cl.'">'.$this->numberFormat($rekord[$field->name]).'</td>';
                        }
                        else
                        {
                            echo '<td class="'.$cl.'">'.$rekord[$field->name].'</td>';
                        }
                    }
                    if (!empty($actions))
                    {
                        echo '<td class="text-right">';                      
                        // ha a paraméterben van 's',akkor megjelenítjük a státuszváltó ikont
                        if (in_array('s', $actions))
                        {
                            echo '
                            <a href="index.php?pg='.base64_encode($originaltablename.'_stch&id='.$rekord[$primary_key]).'" title="Státusz váltás">
                                <svg class="bi" width="20" height="20" fill="currentColor">
                                    <use xlink:href="icons/bootstrap-icons.svg#toggle-'.$icon.'"></use>
                                </svg>
                            </a>';
                        }

                        // ha a paraméterben van 'k',akkor megjelenítjük a kosárba űrlapot
                        if (in_array('k', $actions))
                        {
                            echo $this->toCart($originaltablename, $rekord[$primary_key]);
                            
                        }
                        
                        // ha a paraméterben van 'b',akkor megjelenítjük a részleteket
                        if (in_array('b', $actions))
                        {
                            echo '
                            <a href="index.php?pg='.base64_encode($originaltablename.'_show&id='.$rekord[$primary_key]).'" title="Részletek">
                                <svg class="bi" width="18" height="18" fill="currentColor">
                                    <use xlink:href="icons/bootstrap-icons.svg#info-circle"></use>
                                </svg>
                            </a>';
                        }

                        // ha a paraméterben van 'r',akkor megjelenítjük az alapértelmezett ikont
                        if (in_array('r', $actions))
                        {
                            echo '
                            <a href="index.php?pg='.base64_encode($originaltablename.'_setdef&id='.$rekord[$primary_key]).'" title="Alapértelmezettnek beállít">
                                <svg class="bi" width="16" height="16" fill="currentColor">
                                    <use xlink:href="icons/bootstrap-icons.svg#'.$deficon.'"></use>
                                </svg>
                            </a>';
                        }

                        // ha a paraméterben van 'i',akkor megjelenítjük az információ ikont
                        if (in_array('i', $actions))
                        {
                            echo '
                            <a href="index.php?pg='.base64_encode($originaltablename.'_info&id='.$rekord[$primary_key]).'" title="Rekord információ">
                                <svg class="bi" width="16" height="16" fill="currentColor">
                                    <use xlink:href="icons/bootstrap-icons.svg#info-circle-fill"></use>
                                </svg>
                            </a>';
                        }

                        // ha a paraméterben van 'a',akkor megjelenítjük az attachment ikont
                        if (in_array('a', $actions))
                        {
                            echo '
                            <a href="index.php?pg='.base64_encode($originaltablename.'_attach&id='.$rekord[$primary_key]).'" title="Csatolmány feltöltése">
                                <svg class="bi" width="16" height="16" fill="currentColor">
                                    <use xlink:href="icons/bootstrap-icons.svg#plus-circle-fill"></use>
                                </svg>
                            </a>';
                        }

                        // ha a paraméterben van 'D',akkor megjelenítjük a letöltés ikont
                        if (in_array('D', $actions))
                        {
                            echo '
                            <a href="'.$originaltablename.'/'.$originaltablename.'_download.php?id='.$rekord[$primary_key].'" title="Csatolmány letöltése">
                                <svg class="bi" width="16" height="16" fill="currentColor">
                                    <use xlink:href="icons/bootstrap-icons.svg#arrow-down-circle-fill"></use>
                                </svg>
                            </a>';
                        }

                        // ha a paraméterben van 'u',akkor megjelenítjük a módosítás ikont
                        if (in_array('u', $actions))
                        {
                            echo '
                            <a href="index.php?pg='.base64_encode($originaltablename.'_update&id='.$rekord[$primary_key]).'" title="Rekord módosítás">
                                <svg class="bi" width="16" height="16" fill="currentColor">
                                    <use xlink:href="icons/bootstrap-icons.svg#exclamation-circle-fill"></use>
                                </svg>
                            </a>';
                        }

                          // ha a paraméterben van 'd',akkor megjelenítjük a törlés ikont
                          if (in_array('d', $actions))
                          {
                              echo '
                              <a href="index.php?pg='.base64_encode($originaltablename.'_delete&id='.$rekord[$primary_key]).'" title="Rekord törlés">
                                  <svg class="bi" width="16" height="16" fill="currentColor">
                                      <use xlink:href="icons/bootstrap-icons.svg#x-circle-fill"></use>
                                  </svg>
                              </a>';
                          }                        
                        echo '</td>';
                    }
                    echo '</tr>';
                }
                echo '
                </tbody>
             <!--
                <tfoot>
                    <tr>
                        <td colspan="'.$fieldcount.'">Összesen <strong>'.$rekordcount.'</strong> rekord</td>
                    </tr>
                </tfoot>
            -->
            </table>';
        }

        // a lekérdezés adatainak megjelenítése egy HTML SELECT komponensben
        public function toSelect($table, $valueName, $optionName, $selectedValue, $kondition, $script)
        {
            if (!empty($kondition))
            {
                $kondition = ' WHERE '.$kondition;
            }

            $this->DBquery("SELECT * FROM ".$table.$kondition." ORDER BY ".$optionName." ASC");
            
            $options = '';
            foreach($this->queryresult as $rekord)
            {
                if ($selectedValue == $rekord[$valueName])
                {
                    $selected = ' selected';
                }
                else
                {
                    $selected = '';
                }
                $options .= '<option value="'.$rekord[$valueName].'"'.$selected.'>'.$rekord[$optionName].'</option>';
            }

            return '<select name="'.$table.'" class="form-control" onchange="'.$script.'">
                <option value="">Válasszon...</option>'.$options.'</select>';
        }

        // a lekérdezés adatainak megjelenítése egy HTML GRID komponensben
        public function toGrid($title, $head, $body, $foot, $img, $link)
        {
            echo '<div class="row">';
            foreach($this->queryresult as $record)
            {
                echo '<div class="col-12 col-lg-6 col-xl-4 py-3 px-3 cardbox">
                <div class="card">
                <div class="card-body">
                <h5 class="card-title">'.$record[$title].'</h5>
                <h6 class="card-subtitle mb-2 text-muted">'.$record[$head].'</h6>
                <p class="card-text text-justify">'.$record[$body].'</p>';
                if (!empty($record[$img]))
                {
                    echo '<img src="'.$record[$img].'" class="card-img-top" alt="">';
                }
                else
                {
                    echo '<img src="images/nopic.jpg" class="card-img-top"  alt="">';
                }

                if (!empty($link))
                {  
                    echo '<a href="index.php?pg='.base64_encode($link.'&id='.$record['.aktID']).'" class="btn btn-primary btn-sm">Részletek</a>';
                }
                echo '</div>
                <div class="card-footer text-muted text-center">
                <strong>'.$this->numberFormat($record[$foot]).'</strong>'.$GLOBALS['penznem'];
                
                if(isset($_SESSION['cID']))
                {
                    echo $this->toCart('products', $record['.aktID']);
                }

                echo'</div>
                </div></div>';
            }
            echo '</div>';
        }
        
        // a lekérdezés adatainak megjelenítése egy JS diagramban
        public function toChart($title, $type, $x, $y, $div, $theme, $anim)
        {
            $data_points = '';
            foreach($this->queryresult as $rekord)
            {
                $data_points .= '{ label: "'.htmlspecialchars($rekord[$x]).'",  y: '.$rekord[$y].'  },';
            }
            $data_points = rtrim($data_points, ',');
        

            echo '<script type="text/javascript">
            
            var chart = new CanvasJS.Chart("'.$div.'", {
                theme: "'.$theme.'", // "light1", light2", "dark1", "dark2"
                animationEnabled: '.$anim.', // false or true		
                title:{
                    text: "'.$title.'",
                    fontSize: 23
                },
                axisY:{
                    labelFontSize: 13
                },
                axisX:{
                    labelFontSize: 13
                },
                data: [
                {
                    // Change type to "bar", "area", "spline", "pie",etc.
                    type: "'.$type.'",
                    dataPoints: [
                     '.$data_points.'
                    ]
                }
                ]
            });
            chart.render(); 
            </script>';
        }

        // // a lekérdezés adatainak megjelenítése egy FullCalendar komponensben
        public function toCalendar($title, $start, $end, $value, $div)
        {
            $events = '';
            foreach($this->queryresult as $rekord)
            {
                $str = '';
                $fields = explode('|', $value);
                foreach($fields as $field)
                {
                    if (is_numeric($rekord[$field]))
                    {
                        $str .= $this->numberFormat($rekord[$field]).' ';
                    }
                    else
                    {
                        $str .= $rekord[$field].' ';
                    }    
                }
                $str = rtrim($str, ' ');

                $events .= '{ 
                    title: "'.$str.'",  
                    start: "'.$rekord[$start].'",
                    end:  "'.$rekord[$end].'" 
                },';
            }
            $events = rtrim($events, ',');

            echo "
            <h4>".$title."</h4>
            <hr>
            <script>

            document.addEventListener('DOMContentLoaded', function() {
              var initialLocaleCode = 'hu';
             
              var calendarEl = document.getElementById('".$div."');
          
              var calendar = new FullCalendar.Calendar(calendarEl, {
                headerToolbar: {
                  left: 'prevYear,prev,today,next,nextYear',
                  center: 'title',
                  right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
                },
                initialDate: '".$GLOBALS['today']."',
                locale: initialLocaleCode,
                buttonIcons: true, // show the prev/next text
                weekNumbers: true,
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                dayMaxEvents: true, // allow more link when too many events
                events: [
                  ".$events."
                ]
              });
          
              calendar.render();
          
            });
          
          </script>";
        }

        // a lekérdezés adatainak exportálása PDF-be
        public function toPDF()
        {

        }

        // a lekérdezés adatainak exportálása XLS-be
        public function toXLS()
        {

        }

        //megjelenítjük a kosárba űrlapot
        public function toCart($tablename, $pk)
        {
            echo $this->toForm('name|¤
            action|products_toCart&id='.$pk.'¤
            number|db_||min="0" value="0" style="width:60px"¤
            iconbutton|_cart-plus-fill');
        }

        public function toDashboard($datas)
        {
            echo'<div class="row">';
            foreach($datas as $data)
            {
                echo'
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 py-3 px-3 cardbox">
                <div class="card text-white '.$data->color.' mb-3" style="max-width: 18rem;">
                    <div class="card-header">'.$data->header.'</div>
                    <div class="card-body">
                        <h5 class="card-title text-center">'.$this->numberFormat($data->value).' '.$data->unit.'</h5>
                        '.$data->description.'
                    </div>
                </div>
                </div>';      
            }
            echo'</div>';
        }

        public function toCard($title,$content,$foot)
        {
            echo '
            <div class="col-12 py-3 px-3 cardbox">
                <div class="card">
                    <div class="card-header">
                        '.$title.'
                    </div>
                    <div class="card-body">
                        '.nl2br($content).'
                    </div>
                    <div class="card-footer">
                        '.$foot.'
                    </div>
                </div>
            </div>';
        }

        // egy rekord adatainak megjelenítése HTML táblázatban
        public function showRecord($back)
        {
            $fields = array();
            $fields = $this->queryresult->fetch_fields();
            $fieldcount = $this->connection->field_count;
            $originaltablename = $fields[0]->orgtable;
            $tablename = $fields[0]->table;
            $rekord = $this->fetchOne();
            echo '<h4>'.$tablename.'</h4>
            <hr>
            <table class="table table-striped table-sm table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>Tulajdonság</th>
                        <th>Érték</th>
                    </tr>
                </thead>
                <tbody>';
                $fields = array();
                $fields = $this->queryresult->fetch_fields();
                $fieldcount = $this->connection->field_count;
                $originaltablename = $fields[0]->orgtable;
                $rekord = $this->fetchOne();
                foreach($fields as $field)
                {
                    echo '<tr>
                        <td class="font-weight-bold">'.$field->name.'</td>
                        <td>';
                        if (is_numeric($rekord[$field->name]))
                        {
                            echo $this->numberFormat($rekord[$field->name]);
                        }
                        else
                        {
                            echo $rekord[$field->name];
                        }
                        echo '</td>
                    </tr>';
                }
                echo'
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">Összesen <strong>'.$fieldcount.'</strong> tulajdonság</td>
                    </tr>
                </tfoot>
            </table>';
            if(!empty($back))
            {
                echo '<a href="index.php?pg='.base64_encode($back).'" class="btn btn-primary">Vissza</a>';
            }
            else
            {
                //echo '<a href="index.php?pg='.base64_encode($GLOBALS['default_page']).'" class="btn btn-primary">Vissza</a>';
            }
        }

        // üzenetablak
        public function showMessage($message, $type)
        {
            echo '<div class="alert alert-'.$type.' alert-dismissible fade show animated heartBeat" role="alert">
                '.$message.'
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>';
        }

        // formázott szám kiírás
        public function numberFormat($szam)
        {
            if ($pos = strpos($szam, '.') > 0)
            {           
                return number_format($szam, $GLOBALS['tizedes'], $GLOBALS['tizedeselv'], $GLOBALS['ezreselv']);
            }
            else
            {
                return number_format($szam, 0, $GLOBALS['tizedeselv'], $GLOBALS['ezreselv']);
            }   
        }

        // megadja az elsődleges kulcs nevét
        public function getPrimaryKey($fields)
        {
            $primary_key = '';
			foreach($fields as $field) 
			{
			    if ($field->flags & MYSQLI_PRI_KEY_FLAG) 
			    { 
			        $primary_key = $field->name; 
                    break;
			    }
			}
            return $primary_key;
        }

        // megadja az Set flag mező nevét
        public function getSetFlag($fields)
        {
            $SetFlag = '';
            foreach($fields as $field) 
            {
                if ($field->flags & MYSQLI_SET_FLAG) 
                { 
                    $SetFlag = $field->name; 
                    break;
                }
            }
            return $SetFlag;
        }

        // inputok megtisztítása az escape karakterektől
        public function escapeString($str)
        {
            return $this->connection->real_escape_string($str);
        }

        // belső oldalak védelme
        public function logincheck($session)
        {
            if (!isset($_SESSION[$session]))
            {
                header("location: index.php");
            }
        }

        // auto moderáció
        public function moderate($text, $words)
        {
            foreach($words as $word)
            {
                $mask = '';
                $length = mb_strlen($word);
                for($i = 0; $i < $length; $i++)
                {
                    $mask .= '*';
                }
                $text = str_replace($word, $mask, $text);
            }
            return $text;
        }

        // fájl letöltése
        public function fileDownload($file)
        {
            if (file_exists($file))
            {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                readfile($file);
                exit;
            }
        }

        //fájl feltöltés
        public function fileUpload($mit, $hova, $milyennevre)
        {
            $res = false;
            if(isset($milyennevre))
            {
                if(!is_dir($hova))
                {
                    mkdir($hova);
                }
                if(move_uploaded_file($mit, $hova."/".$milyennevre))
                {
                    $res = true;
                }
                else
                {
                    $res = false;
                }
            }
            return $res;
        }
        
        // szám kiírása szöveggel
        public function num2text($nsz) 
        {
       
            $hatv=array('','ezer','millió','milliárd','billió','billiárd','trillió','trilliárd','kvadrillió','kvadrilliárd','kvintillió','kvintilliárd','szextillió','szextilliárd','szeptillió','szeptilliárd','oktillió','oktilliárd','nonillió','nonilliárd','decillió','decilliárd','centillió');
               
            $tizesek=array('','','harminc','negyven','ötven','hatvan','hetven','nyolcvan','kilencven');
               
            $szamok=array('egy','kettő','három','négy','öt','hat','hét','nyolc','kilenc');
                $tsz='';
                $ej=($nsz<0?'- ':'');
                $sz=trim(''.floor($nsz));
                $hj=0;
                if ($sz=='0') {
                $tsz='nulla';
                } else {
                while ($sz>'') {
                    $hj++;
                    $t='';
                    $wsz=substr('00'.substr($sz,-3),-3);
                    $tizesek[0]=($wsz[2]=='0'?'tíz':'tizen');
                    $tizesek[1]=($wsz[2]=='0'?'húsz':'huszon');
                    if ($c=$wsz[0]) {
                    $t=$szamok[$c-1].'száz';
                    }
                    if ($c=$wsz[1]) {
                    $t.=$tizesek[$c-1];
                    }
                    if ($c=$wsz[2]) {
                    $t.=$szamok[$c-1];
                    }
            //        $tsz=($t?$t.$hatv[$hj-1]:'').($tsz==''?'':'-').$tsz;
            $tsz=($t?$t.$hatv[$hj-1]:'').($tsz==''?'':($nsz>2000?'-':'')).$tsz;
                    $sz=substr($sz,0,-3);
                }
                }
                return $ej.$tsz;
        }
    }   
?>