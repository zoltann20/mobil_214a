    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery_ui.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery.bootstrap-touchspin.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery_ui.js"></script>
    <script src="js/jquery.bootstrap-touchspin.js"></script>
    <script src="js/app.js"></script>
<?php
require("adatok.php");
require("databaseClass.php");
$db = new db($dbhost, $dbname, $dbuser, $dbpass);
    

    $db->DBquery("SELECT * FROM categories");
    $categories = $db->fetchAll();
    echo '
    <div id="accordion">';
        foreach($categories as $cat)
        {
            echo '<h3>'.$cat['name'].'</h3>';
            echo '<div>
                <p>
                <ul>';
                $db->DBquery("SELECT * FROM products WHERE catID=".$cat['ID']."");
                $prod = $db->fetchAll();
                foreach($prod as $p)
                {
                    echo '<li>'.$p['name'].' 
                    <label for="checkbox-'.$p['ID'].'" class="icon"  >
                    <input type="checkbox" onchange="addItem('.$p['ID'].')" id="checkbox-'.$p['ID'].'" class="checkboxes">
                    <svg xmlns="http://www.w3.org/2000/svg" onclick="showTouchSpin('.$p['ID'].');"  width="32" height="32" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                        </svg>
                    </label>
                    <span class="badge badge-primary ">'.$db->numberFormat($p['price']).' '.$GLOBALS['penznem'].'</span>
                    <div class="counters counter-'.$p['ID'].'">
                        <input  id="value-'.$p['ID'].'"
                        type="text"
                            value="1"
                            name="counterTouchspin"
                            data-bts-min="0"
                            data-bts-max="1000"
                            data-bts-init-val=""
                            data-bts-step="1"
                            data-bts-decimal="0"
                            data-bts-step-interval="100"
                            data-bts-force-step-divisibility="round"
                            data-bts-step-interval-delay="500"
                            data-bts-prefix=""
                            data-bts-postfix=""
                            data-bts-prefix-extra-class=""
                            data-bts-postfix-extra-class=""
                            data-bts-booster="true"
                            data-bts-boostat="10"
                            data-bts-max-boosted-step="false"
                            data-bts-mousewheel="true"
                            data-bts-button-down-class="btn btn-danger"
                            data-bts-button-up-class="btn btn-primary"
                            onchange="updateCart('.$p['ID'].');">
                    </div>
                    </li>';
                }
            echo'</ul>
                </p>
            </div>';
        }
    
    echo '
    </div>';
    echo '<label class="summ">Összesen:<span id="summary"></span></label>';
?>